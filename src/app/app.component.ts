import { Component } from '@angular/core';
import {Ingredient} from "./shared/ingredient.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  price = 20;
  array: string[] = [];

  ingredients: Ingredient[] = [
    new Ingredient('https://thumbs.dreamstime.com/b/raw-meat-beef-steak-green-rustic-table-raw-meat-beef-steak-green-rustic-table-background-164197668.jpg', 'Meat', 0, 50),
    new Ingredient('https://thumbs.dreamstime.com/b/swiss-cheese-holes-18911609.jpg', 'Cheese', 0, 20),
    new Ingredient('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUGgEX9agVtp0cxoQksKSK2gGUWJob4XKEhg&usqp=CAU', 'Salad', 0, 5),
    new Ingredient('https://eatitandlikeit.com/wp-content/uploads/2014/01/bacon-1024x1024.jpg', 'Bacon', 0, 30),
  ];

  clickIng(index: number) {
    this.array.push(this.ingredients[index].name);
    this.ingredients[index].amount++;
    this.price = this.price + this.ingredients[index].getPrice();
  }

  deleteButton(index: number) {
    if (this.ingredients[index].amount > 0) {
      this.price = this.price - this.ingredients[index].getPrice();
      this.ingredients[index].amount--;
      if (this.array.includes(this.ingredients[index].name)) {
        const indexArr = this.array.indexOf(this.ingredients[index].name);
        if (indexArr > -1) {
          this.array.splice(indexArr, 1);
        }
      }
    } else {
      return;
    }
  }
}
