export class Ingredient {
  constructor(
    public imageUrl: string,
    public name: string ,
    public amount: number,
    public price: number,
  ) {}

  getPrice() {
    return this.amount * this.price;
  }
}
